package com.example.inventarioAPIRest.Service.impl;

import com.example.inventarioAPIRest.Model.User;
import com.example.inventarioAPIRest.Repository.UserRepository;
import com.example.inventarioAPIRest.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findByEmail( String email ) {
        return userRepository.findByEmail( email );
    }

    @Override
    public List<User> findAllByOrderByName() {
        return userRepository.findAllByOrderByName();
    }

    @Override
    public void saveOrUpdateUser( User user ) {
        userRepository.save( user );
    }

    @Override
    public void deleteUser( String id ) {
        userRepository.deleteById( id );
    }
}
