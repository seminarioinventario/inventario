package com.example.inventarioAPIRest.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Component
@Document(collection = "clothes")
public class Clothes {
    @Id
    private String id;
    private String type;
    private String size;

    public Clothes(String id, String type, String size) {
        this.id = id;
        this.type = type;
        this.size = size;
    }

    public Clothes() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
