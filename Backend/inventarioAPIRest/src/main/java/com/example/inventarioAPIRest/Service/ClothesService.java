package com.example.inventarioAPIRest.Service;

import com.example.inventarioAPIRest.Model.Clothes;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClothesService {
    List<Clothes> findAll();
    Clothes findByType(String type);
    List<Clothes> findAllByOrderByType();
    void saveOrUpdateClothes(Clothes clothes);
    void deleteClothes(String id);
}
