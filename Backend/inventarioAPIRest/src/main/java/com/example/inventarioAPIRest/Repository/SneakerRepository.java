package com.example.inventarioAPIRest.Repository;

import com.example.inventarioAPIRest.Model.Sneaker;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SneakerRepository extends MongoRepository<Sneaker,String> {
    Sneaker findByModel(String model);
    List<Sneaker> findAllByOrderByBrand();
}
