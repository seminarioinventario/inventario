package com.example.inventarioAPIRest.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import com.example.inventarioAPIRest.Model.Sneaker;
import com.example.inventarioAPIRest.Service.SneakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/sneakers")
public class SneakerController {
    @Autowired
    private SneakerService sneakerService;

    @GetMapping(value = "/")
    public List<Sneaker> getAllSneakers() {
        return sneakerService.findAll();
    }

    @GetMapping(value = "/{model}")
    public Sneaker findByModel( @PathVariable("model") String model ) {
        return sneakerService.findByModel( model );
    }

    @GetMapping(value = "/orderByBrand")
    public List<Sneaker> findAllByOrOrderByBrand() {
        return sneakerService.findAllByOrderByBrand();
    }

    @PostMapping(value = "/")
    public ResponseEntity<?> saveOrUpdateSneaker(@RequestBody Sneaker sneaker) {
        sneakerService.saveOrUpdateSneaker( sneaker );
        return new ResponseEntity("Sneaker added successfully", HttpStatus.OK);
    }

    @DeleteMapping(value = "/{sneakerModel}")
    public void deleteUser(@PathVariable("sneakerModel") String model) {
        sneakerService.deleteSneaker(sneakerService.findByModel( model ).getId());
    }
}
