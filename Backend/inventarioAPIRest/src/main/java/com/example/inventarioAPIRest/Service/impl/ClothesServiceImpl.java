package com.example.inventarioAPIRest.Service.impl;

import com.example.inventarioAPIRest.Model.Clothes;
import com.example.inventarioAPIRest.Repository.ClothesRepository;
import com.example.inventarioAPIRest.Service.ClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClothesServiceImpl implements ClothesService {
    @Autowired
    private ClothesRepository clothesRepository;

    @Override
    public List<Clothes> findAll() {
        return clothesRepository.findAll();
    }

    @Override
    public Clothes findByType( String type ) { return clothesRepository.findByType( type ); }

    @Override
    public List<Clothes> findAllByOrderByType() { return clothesRepository.findAllByOrderByType(); }

    @Override
    public void saveOrUpdateClothes( Clothes clothes ) {
        clothesRepository.save( clothes );
    }

    @Override
    public void deleteClothes( String id ) {
        clothesRepository.deleteById( id );
    }
}
