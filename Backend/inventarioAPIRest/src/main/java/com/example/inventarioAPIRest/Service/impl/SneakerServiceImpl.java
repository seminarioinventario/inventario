package com.example.inventarioAPIRest.Service.impl;

import com.example.inventarioAPIRest.Model.Sneaker;
import com.example.inventarioAPIRest.Repository.SneakerRepository;
import com.example.inventarioAPIRest.Service.SneakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SneakerServiceImpl implements SneakerService {
    @Autowired
    private SneakerRepository sneakerRepository;

    @Override
    public List<Sneaker> findAll() {
        return sneakerRepository.findAll();
    }

    @Override
    public Sneaker findByModel( String model ) { return sneakerRepository.findByModel( model ); }

    @Override
    public List<Sneaker> findAllByOrderByBrand() { return sneakerRepository.findAllByOrderByBrand(); }

    @Override
    public void saveOrUpdateSneaker( Sneaker sneaker ) {
        sneakerRepository.save( sneaker );
    }

    @Override
    public void deleteSneaker( String id ) {
        sneakerRepository.deleteById( id );
    }
}
