package com.example.inventarioAPIRest.Service;

import com.example.inventarioAPIRest.Model.Sneaker;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SneakerService {
    List<Sneaker> findAll();
    Sneaker findByModel(String model);
    List<Sneaker> findAllByOrderByBrand();
    void saveOrUpdateSneaker(Sneaker sneaker);
    void deleteSneaker(String id);
}
