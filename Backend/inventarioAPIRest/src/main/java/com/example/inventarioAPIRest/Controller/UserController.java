package com.example.inventarioAPIRest.Controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.example.inventarioAPIRest.Model.User;
import com.example.inventarioAPIRest.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/")
    public List<User> getAllUsers() {
        System.out.println("buenas");
        return userService.findAll();
    }

    @GetMapping(value = "/hello")
    public ResponseEntity<?> getHello() {
        return new ResponseEntity("hello world", HttpStatus.OK);
    }

    @GetMapping(value = "/{email}")
    public User getUserByEmail( @PathVariable("email") String email ) {
        return userService.findByEmail( email );
    }

    @GetMapping(value = "/orderByName")
    public List<User> findAllByOrderByName() {
        return userService.findAllByOrderByName();
    }

    @PostMapping(value = "/")
    public ResponseEntity<?> saveOrUpdateUser(@RequestBody User user) {
        System.out.println(user);
        userService.saveOrUpdateUser( user );
        return new ResponseEntity("User added successfully", HttpStatus.OK);
    }

    @DeleteMapping(value = "/{userEmail}")
    public void deleteUser(@PathVariable("userEmail") String email) {
        userService.deleteUser(userService.findByEmail( email ).getId());
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        try {
            User findUser = userService.findByEmail( user.getEmail() );
            System.out.println("user:"+findUser);
            if ( !findUser.getPassword().equals(user.getPassword()) ) {
                return new ResponseEntity<String>("email or password is wrong",HttpStatus.BAD_REQUEST);
            }
            else {
                String token = getJWTToken(user.getEmail());
                JSONObject response = new JSONObject();
                response.put("token", token);
                System.out.println(response);
                return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
            }
        }
        catch ( NullPointerException e ) {
            return new ResponseEntity<String>("email or password is wrong", HttpStatus.BAD_REQUEST);
        }
    }

    private String getJWTToken(String email) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("softtekJWT")
                .setSubject(email)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 6000000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }

}
