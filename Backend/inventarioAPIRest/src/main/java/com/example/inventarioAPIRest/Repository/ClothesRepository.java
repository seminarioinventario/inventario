package com.example.inventarioAPIRest.Repository;

import com.example.inventarioAPIRest.Model.Clothes;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClothesRepository extends MongoRepository<Clothes,String> {
    Clothes findByType(String type);
    List<Clothes> findAllByOrderByType();
}
