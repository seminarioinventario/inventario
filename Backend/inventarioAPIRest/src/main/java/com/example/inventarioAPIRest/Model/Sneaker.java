package com.example.inventarioAPIRest.Model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Component
@Document(collection = "sneakers")
public class Sneaker {
    @Id
    private String id;
    private String brand;
    private String model;
    private String size;

    public Sneaker(String id, String brand, String model, String size) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.size = size;
    }

    public Sneaker() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
