package com.example.inventarioAPIRest.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import com.example.inventarioAPIRest.Model.Clothes;
import com.example.inventarioAPIRest.Service.ClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/clothes")
public class ClothesController {
    @Autowired
    private ClothesService clothesService;

    @GetMapping(value = "/")
    public List<Clothes> getAllSneakers() {
        return clothesService.findAll();
    }

    @GetMapping(value = "/{type}")
    public Clothes findByModel( @PathVariable("type") String type ) {
        return clothesService.findByType( type );
    }

    @GetMapping(value = "/orderByBrand")
    public List<Clothes> findAllByOrOrderByBrand() {
        return clothesService.findAllByOrderByType();
    }

    @PostMapping(value = "/")
    public ResponseEntity<?> saveOrUpdateSneaker(@RequestBody Clothes clothes) {
        clothesService.saveOrUpdateClothes( clothes );
        return new ResponseEntity("Clothes added successfully", HttpStatus.OK);
    }

    @DeleteMapping(value = "/{clothesType}")
    public void deleteUser(@PathVariable("clothesType") String type) {
        clothesService.deleteClothes(clothesService.findByType( type ).getId());
    }
}
