import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Zapatos',
      name: 'Zapatos',
      component: () => import(/* webpackChunkName: "about" */ '../views/Zapatos.vue')
  },
  {
    path: '/Ropa',
      name: 'Ropa',
      component: () => import(/* webpackChunkName: "about" */ '../views/Ropa.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router