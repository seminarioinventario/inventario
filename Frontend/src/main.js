import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Styles from './assets/master.css'
import vuetify from './plugins/vuetify'
import colors from 'vuetify/lib/util/colors'

Vue.config.productionTip = false

new Vue({
  router,
  Styles,
  vuetify,
  theme: {
    themes: {
      light: {
        primary: colors.red.darken1, // #E53935
        secondary: colors.red.lighten4, // #FFCDD2
        accent: colors.indigo.base, // #3F51B5
      },
    },
  },
  render: h => h(App)
}).$mount('#app')
